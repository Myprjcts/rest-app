package com.simple.simplerestapp;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Mohammad Abusbeih
 */
@RestController
@RequestMapping(value = "/app")
public class AppController {

    @GetMapping(value = "/message")
    public ResponseEntity<Map<String, String>> helloApi() {
        HashMap<String, String> map = new HashMap<>();

        map.put("Hi Message", "Hello CI/CD Done Successfully !!");

        return ResponseEntity.status(HttpStatus.OK).body(map);
    }
}
